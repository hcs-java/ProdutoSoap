/**
 * Data de criacao 26/06/2012
 * Projeto : LoginTRFWARV1
 *
 * Copyright 2012 - Fleury.
 * Brasil
 * Todos os direitos reservados.
 *
 * Este software e propriedade do Grupo Fleury.
 *
 */
package br.com.grupofleury.produtoent.core.exception;

/**
 * <P><B>Descricao :</B><BR>
 * Classe base para excecao do tipo nao checada.
 * </P>
 * <P>
 * <B>
 * Pontos de atencao : <BR>
 * Nenhum
 * </B>
 * @author A2F
 * @since 26/06/2012
 * @version 1.0
 */
public class ProdutoRuntimeException extends RuntimeException {
	
	/**
	 * Numero que indentifica a versao da classe.
	 */
	private static final long serialVersionUID = 7644711022573624242L;
	
	/**
	 * Construtor padrão.
	 */
	public ProdutoRuntimeException() {
		super();
	}
	
	/**
	 * Chama o construtor da classe Exception.
	 *
	 * @param message Mensagem do erro <code>String</code>
	 * @param cause Causa que originou o problema <code>Throwable</code>
	 *
	 */
	public ProdutoRuntimeException(final String message, final Throwable cause) {
		super(message, cause);
	}

	/**
	 * Chama o construtor da classe Exception.
	 *
	 * @param message Mensagem do erro <code>String</code>
	 *
	 */
	public ProdutoRuntimeException(final String message) {
		super(message);
	}

	/**
	 * Chama o construtor da classe Exception.
	 *
	 * @param cause Causa que originou o problema <code>Throwable</code>.
	 *
	 */
	public ProdutoRuntimeException(final Throwable cause) {
		super(cause);
	}


}
