/**
 * Data de criação 08/08/2013
 * Projeto: ProdutoENTWARV1
 *
 * Copyright 2013 - Fleury.
 * Brasil
 * Todos os direitos reservados.
 *
 * Este software é propriedade do Grupo Fleury.
 *
 */
package br.com.grupofleury.produtoent.web.webservices;

import javax.jws.WebService;

import br.com.grupofleury.servicos.fleury.mc.v1.produto.ListaProduto;
import br.com.grupofleury.servicos.fleury.produto.produtoent.v1.BuscarDetalhesProdutoRequest;
import br.com.grupofleury.servicos.fleury.produto.produtoent.v1.BuscarDetalhesProdutoResponse;
import br.com.grupofleury.servicos.fleury.produto.produtoent.v1.Excecao;
import br.com.grupofleury.servicos.fleury.produto.produtoent.v1.ListarInstrucoesGeraisRequest;
import br.com.grupofleury.servicos.fleury.produto.produtoent.v1.ListarInstrucoesGeraisResponse;
import br.com.grupofleury.servicos.fleury.produto.produtoent.v1.ListarInstrucoesPersonalizadasRequest;
import br.com.grupofleury.servicos.fleury.produto.produtoent.v1.ListarInstrucoesPersonalizadasResponse;
import br.com.grupofleury.servicos.fleury.produto.produtoent.v1.ListarProdutoPorUnidadeRequest;
import br.com.grupofleury.servicos.fleury.produto.produtoent.v1.ListarProdutoPorUnidadeResponse;
import br.com.grupofleury.servicos.fleury.produto.produtoent.v1.ListarProdutoRequest;
import br.com.grupofleury.servicos.fleury.produto.produtoent.v1.ListarProdutoResponse;
import br.com.grupofleury.servicos.fleury.produto.produtoent.v1.ProdutoENTPortType;
import br.com.grupofleury.web.webservices.common.AbstractBaseWebService;

/**
 * @author heros.silva
 *
 */
@WebService(serviceName = "ProdutoService", endpointInterface = "br.com.grupofleury.servicos.fleury.produto.produtoent.v1.ProdutoENTPortType")
public class ProdutoWSImpl extends AbstractBaseWebService implements ProdutoENTPortType {

	@Override
	public BuscarDetalhesProdutoResponse buscarDetalhesProduto(BuscarDetalhesProdutoRequest in) throws Excecao {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ListarProdutoResponse listarProduto(ListarProdutoRequest in) throws Excecao {
		// TODO Auto-generated method stub

		ListarProdutoResponse response = new ListarProdutoResponse();

		ListaProduto list = new ListaProduto();

		response.setListaProduto(list);

		return response;
	}

	@Override
	public ListarProdutoPorUnidadeResponse listarProdutoPorUnidade(ListarProdutoPorUnidadeRequest in) throws Excecao {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ListarInstrucoesGeraisResponse listarInstrucoesGerais(ListarInstrucoesGeraisRequest in) throws Excecao {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ListarInstrucoesPersonalizadasResponse listarInstrucoesPersonalizadas(
			ListarInstrucoesPersonalizadasRequest in) throws Excecao {
		// TODO Auto-generated method stub
		return null;
	}

}
